package com.example.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.time.ZonedDateTime;

public class Todo {
    private int id;

    @NotEmpty
    private String text;

    private Status status;

    @NotNull
    private ZonedDateTime dueDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ZonedDateTime getDueDate() { return dueDate; }

    public void setDueDate(ZonedDateTime dueDate) { this.dueDate = dueDate; }
}
