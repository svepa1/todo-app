package com.example.dto;

public record ErrorMessage(String error) {}
