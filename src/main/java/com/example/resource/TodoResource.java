package com.example.resource;

import com.example.dto.ErrorMessage;
import com.example.dto.InfoMessage;
import com.example.dto.Todo;
import com.example.service.TodoService;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("todos")
@Singleton
public class TodoResource {

    private final TodoService todoService;

    @Inject
    public TodoResource(TodoService todoService) {
        this.todoService = todoService;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTodos() {
        return Response.ok().entity(todoService.getTodos()).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addNewTodo(@NotNull @Valid Todo newTodo) {
        todoService.addNewTodo(newTodo);

        return Response.status(Response.Status.CREATED).entity(newTodo).build();
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateTodo(@PathParam("id") int id, Todo todo) {
        if (id != todo.getId()) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorMessage("The resource id and the body" +
                    " todo id do not match")).build();
        }

        if (todoService.updateTodo(todo)) {
            return Response.ok().build();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteTodo(@PathParam("id") int id) {
        if (todoService.deleteTodo(id)) {
            return Response.ok().build();
        }

        return Response.status(Response.Status.NOT_FOUND).entity(new InfoMessage("The todo with id %d does not exist".formatted(id))).build();
    }
}
