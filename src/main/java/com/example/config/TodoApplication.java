package com.example.config;

import com.example.service.InMemoryTodoService;
import com.example.service.TodoService;
import jakarta.ws.rs.ApplicationPath;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

@ApplicationPath("api")
public class TodoApplication extends ResourceConfig {

    public TodoApplication() {
        init();
    }

    private void init() {
        packages("com.example");
        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(InMemoryTodoService.class).to(TodoService.class);
            }
        });

        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
    }
}
