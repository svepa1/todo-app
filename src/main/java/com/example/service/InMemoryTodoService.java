package com.example.service;

import com.example.dto.Status;
import com.example.dto.Todo;
import org.jvnet.hk2.annotations.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class InMemoryTodoService implements TodoService {

    public static final int MAX_SIZE = 250;
    AtomicInteger nextTodoId = new AtomicInteger(1);
    private List<Todo> todos = new ArrayList<>();

    public List<Todo> getTodos() {
        return Collections.unmodifiableList(todos);
    }

    public void addNewTodo(Todo todo) {
        if (todos.size() == MAX_SIZE) {
            todos.remove(0);
        }

        todo.setId(nextTodoId.getAndIncrement());
        todo.setStatus(Status.NEW);
        todos.add(todo);
    }

    public boolean updateTodo(Todo todo) {
        Optional<Todo> existingTodo = todos.stream().filter(t -> t.getId() == todo.getId()).findAny();
        existingTodo.ifPresent(t -> {
            t.setStatus(todo.getStatus());
            t.setText(todo.getText());
        });

        return existingTodo.isPresent();
    }

    public boolean deleteTodo(int id) {
        return todos.removeIf(t -> t.getId() == id);
    }
}
