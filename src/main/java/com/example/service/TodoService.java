package com.example.service;


import com.example.dto.Todo;
import org.jvnet.hk2.annotations.Contract;

import java.util.List;

@Contract
public interface TodoService {
    List<Todo> getTodos();

    void addNewTodo(Todo todo);

    boolean updateTodo(Todo todo);

    boolean deleteTodo(int id);
}
