package com.example;

import com.example.config.TodoApplication;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.StaticHttpHandler;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;

import java.io.IOException;
import java.net.URI;

public class Main {
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:8080/";

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     *
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI),
                new TodoApplication());

        configureDevelopmentMode(server);
        return server;
    }

    private static void configureDevelopmentMode(HttpServer server) {
        final StaticHttpHandler staticHttpHandler = new StaticHttpHandler("src/main/webapp");
        staticHttpHandler.setFileCacheEnabled(false);
        server.getServerConfiguration().addHttpHandler(staticHttpHandler, "/");
        printDevModeBanner();
    }

    private static void printDevModeBanner() {
        System.out.println("""
                        -------------------------------------------------------------------------------------------------------------
                        -                                                                                                           -
                        -         _____                                   __             __           __   _                        -
                        -        / ___/___  ______   _____  _____   _____/ /_____ ______/ /____  ____/ /  (_)___                    -
                        -        \\__ \\/ _ \\/ ___/ | / / _ \\/ ___/  / ___/ __/ __ `/ ___/ __/ _ \\/ __  /  / / __ \\                   -
                        -       ___/ /  __/ /   | |/ /  __/ /     (__  ) /_/ /_/ / /  / /_/  __/ /_/ /  / / / / /                   -
                        -      /____/\\___/_/    |___/\\___/_/     /____/\\__/\\__,_/_/   \\__/\\___/\\__,_/  /_/_/ /_/                    -
                        -          ____  _______    ______________  ____  __  __________   ________                       __        -
                        -         / __ \\/ ____/ |  / / ____/ / __ \\/ __ \\/  |/  / ____/ | / /_  __/  ____ ___  ____  ____/ /__      -
                        -        / / / / __/  | | / / __/ / / / / / /_/ / /|_/ / __/ /  |/ / / /    / __ `__ \\/ __ \\/ __  / _ \\     -
                        -       / /_/ / /___  | |/ / /___/ / /_/ / ____/ /  / / /___/ /|  / / /    / / / / / / /_/ / /_/ /  __/     -
                        -      /_____/_____/  |___/_____/_/\\____/_/   /_/  /_/_____/_/ |_/ /_/    /_/ /_/ /_/\\____/\\__,_/\\___/      -
                        -                                                                                                           -
                        -------------------------------------------------------------------------------------------------------------                            
                            """);
    }

    /**
     * Main method.
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();

        System.out.println(String.format("Jersey app started with endpoints available at "
                + "%s%nHit Ctrl-C to stop it...", BASE_URI));
        System.in.read();
        server.shutdown();
    }
}

