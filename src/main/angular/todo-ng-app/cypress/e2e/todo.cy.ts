const pageUrl = 'http://localhost:8080/';
const MAX_DELAY = 500;

describe('Test the todo app', () => {
  it('should checks that there are no todos yet', () => {
    cy.visit(pageUrl);
    cy.get('#todos-list').should('exist').children().should('have.length', 0);
  })

  it('should check that no todo is added without text', function () {
    cy.visit(pageUrl);
    cy.get('.ph-paper-plane-right-thin').click().wait(MAX_DELAY);
    cy.get('#todos-list').should('exist').children().should('have.length', 0);
  });

  it('should add a todo with current date as due date', function () {
    cy.visit(pageUrl);
    let todoText = 'Den Hund füttern';
    cy.get('#new-todo-text').focus().type(todoText).type('{enter}').wait(MAX_DELAY);
    cy.get('#todos-list').should('exist').children().should('have.length', 1);
    let today = new Date();
    let year = today.getFullYear();
    let month = today.getMonth() + 1;
    let date = today.getDate();
    let todayString = (date + '').padStart(2, '0') + '.' + (month + '').padStart(2, '0') + '.' + year
    cy.get('#todos-list').should('exist').children().first()
      .should('contain.text', 'Fällig am: ' + todayString)
      .should('contain.text', todoText);
    cy.get('#todos-list .btn.btn-lg').first().should('exist').should('have.class', 'btn-main');
  });

  it('should add a second todo with due date in the far future', function () {
    cy.visit(pageUrl);
    let todoText = 'Neues Auto bestellen';
    cy.get('#new-todo-text').focus().type(todoText);
    cy.get('#new-todo-due-date').focus().type('2028-01-03');
    cy.get('.ph-paper-plane-right-thin').click().wait(MAX_DELAY);
    cy.get('#todos-list').should('exist').children().should('have.length', 2);
  });

  it('should mark the first todo as done', function () {
    cy.visit(pageUrl);
    cy.get('#todos-list .ph-square-thin').should('have.length', 2);
    cy.get('#todos-list .ph-square-thin').first().click().wait(MAX_DELAY).should('not.exist');
    cy.get('#todos-list .btn.btn-lg').first().should('exist').should('have.class', 'bg-emphasized');
    cy.get('#todos-list .ph-square-thin').should('have.length', 1);
    cy.get('#todos-list .ph-check-square-thin').should('have.length', 1);
  });

  it('should remove all todos', function () {
    cy.visit(pageUrl);
    cy.get('#todos-list .ph-x-thin').first().click().wait(MAX_DELAY);
    cy.get('#todos-list').should('exist').children().should('have.length', 1);
    cy.get('#todos-list .ph-x-thin').first().click().wait(MAX_DELAY);
    cy.get('#todos-list').should('exist').children().should('have.length', 0);
  });
})
