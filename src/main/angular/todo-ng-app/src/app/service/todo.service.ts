import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {Todo} from "../model/todo";
import {Mode} from "../model/mode";
import {ITodoService} from "./i-todo.service";

@Injectable({
  providedIn: 'root'
})
export class TodoService implements ITodoService {

  todos: Todo[] = [];

  mode: Mode = Mode.ONLINE;

  private _TODOS_PATH_: string = 'api/todos';

  constructor(private httpClient: HttpClient) {
  }

  getTodos(): void {
    this.httpClient.get<Todo[]>(this._TODOS_PATH_).subscribe((todos: Todo[]) => this.todos = todos);
  }

  addTodo(todo: Todo): void {
    this.httpClient.post(this._TODOS_PATH_, todo, {observe: "response"}).subscribe({
      next: (res: HttpResponse<Todo>) => {
        this.todos.push(res.body as Todo);
        todo.text = '';
        todo.dueDate = new Date();
        setTimeout(() => document.getElementById('todos-list')?.scrollTo({ top: 10000, behavior: "smooth" }), 500);
      },
      error: (err: any) => console.log('addition failed', err)
    });
  }

  deleteTodo(todo: Todo, idx?: number): void {
    this.httpClient.delete(this._TODOS_PATH_ + `/${todo.id}`, {observe: 'response'}).subscribe({
      next: () => {
        if (typeof idx !== 'undefined') {
          todo.id = -1;
          setTimeout(() => this.todos.splice(idx, 1), 400);
        } else {
          // TODO: Implement deletion by ID
        }
      },
      error: (err: any) => console.log('deletion failed', err)
    });
  }

  updateTodo(todo: Todo): void {
    this.httpClient.put(this._TODOS_PATH_ + `/${todo.id}`, todo, {observe: 'response'}).subscribe({
      error: (err: any) => console.log('update failed', err)
    });
  }
}
