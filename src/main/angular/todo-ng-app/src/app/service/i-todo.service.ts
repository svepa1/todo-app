import {Todo} from "../model/todo";

export interface ITodoService {
  todos: Todo[];
  getTodos(): void;
  addTodo(todo: Todo): void;
  updateTodo(todo: Todo): void;
  deleteTodo(todo: Todo, idx?: number): void;
}
