export class Todo {
  id?: number;

  text?: string;

  status?: string;

  dueDate?: Date = new Date();
}
