import {Component, Input} from '@angular/core';
import {Todo} from "../../model/todo";
import {Status} from "../../model/status";
import {TodoService} from "../../service/todo.service";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.sass']
})
export class TodoComponent {
  status = Status;

  @Input('todo')
  todo: Todo = {};

  @Input('idx')
  idx?: number;

  constructor(private todoService: TodoService) {
  }

  delete(event: any): void {
    event.stopPropagation();
    this.todoService.deleteTodo(this.todo, this.idx);
  }

  changeStatus(): void {
    if (this.todo) {
      if (this.todo.status === Status.NEW) {
        this.todo.status = Status.DONE;
        this.todoService.updateTodo(this.todo);
      }
    }
  }
}
