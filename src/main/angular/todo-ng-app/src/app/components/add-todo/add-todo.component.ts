import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {TodoService} from "../../service/todo.service";
import {Todo} from "../../model/todo";

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.sass']
})
export class AddTodoComponent implements AfterViewInit {

  todo: Todo = new Todo();

  @ViewChild('newTodoTextInput', {static: false}) newTodoTextInput: ElementRef | undefined;

  constructor(private todoService: TodoService) {
  }

  addTodo() {
    if (this.todo?.text?.length === 0) {
      return;
    }

    this.todoService.addTodo(this.todo);
  }

  ngAfterViewInit(): void {
    this.newTodoTextInput?.nativeElement.focus();
  }

  transformValue(event: any) {
    console.log('transform')
    if (event.target.value) {
      this.todo.dueDate = new Date(event.target.value);
    }
  }
}
