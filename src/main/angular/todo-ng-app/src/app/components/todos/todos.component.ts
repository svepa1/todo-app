import {Component, OnInit} from '@angular/core';
import {Todo} from "../../model/todo";
import {TodoService} from "../../service/todo.service";

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.sass']
})
export class TodosComponent implements OnInit {

  constructor(private todoService: TodoService) {

  }

  ngOnInit(): void {
    this.loadTodos();
  }

  loadTodos(): void {
    this.todoService.getTodos();
  }

  getTodos(): Todo[] {
    return this.todoService.todos;
  }

  newTodos() {
    return this.todoService.todos.filter((todo: Todo) => todo.status + `` == 'NEW');
  }

  finishedTodos() {
    return this.todoService.todos.filter((todo: Todo) => todo.status + `` == 'DONE');
  }
}
