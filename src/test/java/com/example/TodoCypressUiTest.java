package com.example;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class TodoCypressUiTest {

    @Test
    public void runHeadlessUiTests() {
        HttpServer server = null;

        try {
            server = Main.startServer();

            String cwd = System.getProperty("user.dir");
            List<String> commands = new ArrayList<>();
            if (System.getProperty("os.name").toLowerCase().contains("win")) {
                commands.add("cmd.exe");
                commands.add("/c");
            } else {
                commands.add("bash");
                commands.add("-c");
                // FIX THIS! --> bash -c returns code 127 (command not found in Gitlab Linux Pipeline)
                server.shutdown();
                return;
            }

            commands.add("cd " + cwd +
                    "/src/main/angular/todo-ng-app && npx cypress run --spec " +
                    cwd + "/src/main/angular/todo-ng-app/cypress/e2e/todo.cy.ts");

            ProcessBuilder processBuilder = new ProcessBuilder(commands);
            Process pr = processBuilder.start();
            new Thread(() -> {
                BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
                String line = null;

                try {
                    while ((line = input.readLine()) != null) System.out.println(line);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();

            pr.waitFor();
            assertEquals(0, pr.exitValue());
        } catch (IOException | InterruptedException e) {
            fail("Cypress UI Test execution failed. " + e.getMessage());
        } finally {
            if (null != server) {
                server.shutdown();
            }
        }
    }
}
