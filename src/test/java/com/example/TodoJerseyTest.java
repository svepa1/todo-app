package com.example;

import com.example.config.TodoApplication;
import jakarta.ws.rs.core.Application;
import org.glassfish.jersey.test.JerseyTest;

public class TodoJerseyTest extends JerseyTest {

    @Override
    protected Application configure() {
        set("jersey.config.test.container.port", 8080);

        return new TodoApplication();
    }
}
