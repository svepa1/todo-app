package com.example;

import com.example.config.TodoApplication;
import com.example.dto.ErrorMessage;
import com.example.dto.InfoMessage;
import com.example.dto.Todo;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TodoResourceTest extends TodoJerseyTest {

    static final String TODOS_PATH = "/api/todos";

    @Test
    void testGetAllTodos() {
        assertEquals(0, getAllTodos().size());
    }

    @Test
    void testAddNewTodo_success() {
        Todo todo = new Todo();
        todo.setText("I am a Todo");
        todo.setDueDate(ZonedDateTime.now());

        Response response = addTodo(todo);
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        List<Todo> todos = getAllTodos();
        assertEquals(1, todos.size());
        assertEquals(1, todos.get(0).getId());
    }

    @Test
    void testAddNewTodo_nullObject() {
        Response response = addTodo(null);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertEquals(0, getAllTodos().size());
    }

    @Test
    void testAddNewTodo_missingText() {
        Todo todo = new Todo();
        todo.setDueDate(ZonedDateTime.now());
        Response response = addTodo(todo);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertEquals(0, getAllTodos().size());
    }

    @Test
    void testAddNewTodo_missingDueDate() {
        Todo todo = new Todo();
        todo.setText("Todo Text");
        Response response = addTodo(todo);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertEquals(0, getAllTodos().size());
    }

    @Test
    void testUpdateTodo_success() {
        Todo todo = new Todo();
        todo.setText("I am a Todo");
        todo.setDueDate(ZonedDateTime.now());

        Response response = addTodo(todo);
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        List<Todo> todos = getAllTodos();
        assertEquals(1, todos.size());
        assertEquals(1, todos.get(0).getId());

        String newText = "I am an awesome Todo";
        todos.get(0).setText(newText);
        response = updateTodo(todos.get(0).getId(), todos.get(0));
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        todos = getAllTodos();
        assertEquals(1, todos.size());
        assertEquals(1, todos.get(0).getId());
        assertEquals(newText, todos.get(0).getText());
    }

    @Test
    void testUpdateTodo_idMismatch() {
        Todo todo = new Todo();
        todo.setText("I am a Todo");
        todo.setDueDate(ZonedDateTime.now());

        Response response = addTodo(todo);
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        Todo todoFromResponse = response.readEntity(Todo.class);
        response = updateTodo(todoFromResponse.getId() + 4711, todoFromResponse);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        ErrorMessage errMessage = response.readEntity(ErrorMessage.class);
        assertTrue(errMessage != null && !errMessage.error().isEmpty());
    }

    @Test
    void testUpdateTodo_notFound() {
        Todo todo = new Todo();
        todo.setId(4711);
        Response response = updateTodo(todo.getId(), todo);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    void testDeleteTodo_notFound() {
        Response response = deleteTodo(4711);
        InfoMessage infoMessage = response.readEntity(InfoMessage.class);
        assertTrue(infoMessage != null && !infoMessage.info().isEmpty());
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    void testDeleteTodo_success() {
        Todo todo = new Todo();
        todo.setText("I am a Todo");
        todo.setDueDate(ZonedDateTime.now());

        Response response = addTodo(todo);
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        List<Todo> todos = getAllTodos();
        assertEquals(1, todos.size());
        assertEquals(1, todos.get(0).getId());

        response = deleteTodo(todos.get(0).getId());
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        todos = getAllTodos();
        assertEquals(0, todos.size());
    }

    private List<Todo> getAllTodos() {
        return target(TODOS_PATH).request(MediaType.APPLICATION_JSON).get(new GenericType<List<Todo>>() {
        });
    }

    private Response addTodo(Todo todo) {
        return target(TODOS_PATH).request().post(Entity.entity(todo, MediaType.APPLICATION_JSON));
    }

    private Response updateTodo(int id, Todo todo) {
        return target(TODOS_PATH + "/" + id).request().put(Entity.entity(todo, MediaType.APPLICATION_JSON));
    }

    private Response deleteTodo(int id) {
        return target(TODOS_PATH + "/" + id).request().delete();
    }
}